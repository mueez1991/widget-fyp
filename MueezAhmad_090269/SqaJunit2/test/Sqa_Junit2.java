/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Test;
import static org.junit.Assert.*;
import sqaaa.Sqa_class;

/**
 *
 * @author mueez91
 */
public class Sqa_Junit2 {
    
    public Sqa_Junit2() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void test1(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",3,obj.addFunction(1, 2));
    }
    
    @Test
    public void test2(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",4,obj.addFunction(2, -6));
    }
    
    @Test
    public void test3(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",4,obj.addFunction('r', 'e'));
    }
    
    @Test
    public void test4(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",4,obj.addFunction('@', '#'));
    }
    
    @Test
    public void test5(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",10,obj.addFunction(15, -5));
    }
    
    @Test
    public void test6(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",10,obj.subFunction(15, 5));
    }
    
    @Test
    public void test7(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",4,obj.subFunction(-6, -10));
    }
    
    @Test
    public void test8(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",4,obj.subFunction('w', 'f'));
    }
    
    @Test
    public void test9(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",4,obj.subFunction('@', '#'));
    }
    
    @Test
    public void test10(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",-10,obj.subFunction(-15, -5));
    }
    
    @Test
    public void test11(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",-10,obj.addFunction(0.67, 0.45));
    }
    
    @Test
    public void test12(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",-10,obj.subFunction(0.76, 0.56));
    }
    
    @Test
    public void test13(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",20,obj.mulFunction(2, 10));
    }
    
    @Test
    public void test14(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",50,obj.mulFunction(-10, -5));
    }
    
    @Test
    public void test15(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",-50,obj.mulFunction(-10, 5));
    }
    
    @Test
    public void test16(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",-10,obj.mulFunction('t', 'u'));
    }
    
    @Test
    public void test17(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",-10,obj.mulFunction('*', '^'));
    }
    
    @Test
    public void test18(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",0,obj.mulFunction(0.87, 0.54));
    }
    
     @Test
    public void test19(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",5,obj.divFunction(10, 2));
    }
    
     @Test
    public void test20(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",-5,obj.divFunction(-10, 2));
    }
    
     @Test
    public void test21(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",5,obj.divFunction(-10, -2));
    }
    
     @Test
    public void test22(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",20,obj.divFunction('a', 'e'));
    }
    
     @Test
    public void test23(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",20,obj.divFunction('!', '$'));
    }
    
     @Test
    public void test24(){
        Sqa_class obj=new Sqa_class();
        assertEquals("Result",1,obj.divFunction(10, 0));
    }
    
    
    
    
    
    
}
