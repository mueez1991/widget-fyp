package gui.views;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Font;
import javax.swing.JTextField;

public class Calculator extends JInternalFrame {
	private JTextField textField;

	/**
	 * Create the frame.
	 */
	public Calculator() {
		setTitle("Calculator");
		setFrameIcon(new ImageIcon(
				Calculator.class.getResource("/gui/icons/Calculator-icon.png")));
		setClosable(true);
		setBounds(100, 100, 301, 240);
		getContentPane().setLayout(null);

		JButton btnNewButton = new JButton("+");
		btnNewButton.setBounds(32, 45, 48, 34);
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnNewButton.setBackground(new Color(102, 102, 153));
		getContentPane().add(btnNewButton);

		JButton button = new JButton("-");
		button.setFont(new Font("Tahoma", Font.BOLD, 12));
		button.setBackground(new Color(102, 102, 153));
		button.setBounds(118, 45, 48, 34);
		getContentPane().add(button);

		JButton button_1 = new JButton("/");
		button_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		button_1.setBackground(new Color(102, 102, 153));
		button_1.setBounds(76, 108, 48, 34);
		getContentPane().add(button_1);

		JButton btnX = new JButton("x");
		btnX.setFont(new Font("Tahoma", Font.BOLD, 12));
		btnX.setBackground(new Color(102, 102, 153));
		btnX.setBounds(209, 45, 48, 34);
		getContentPane().add(btnX);

		JButton button_2 = new JButton("=");
		button_2.setFont(new Font("Tahoma", Font.BOLD, 12));
		button_2.setBackground(new Color(102, 102, 153));
		button_2.setBounds(168, 108, 48, 34);
		getContentPane().add(button_2);

		textField = new JTextField();
		textField.setBounds(95, 166, 104, 34);
		getContentPane().add(textField);
		textField.setColumns(10);

	}
}
