package gui.views;

import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;

public class OperandGetter extends JInternalFrame {

	/**
	 * Create the frame.
	 */
	public OperandGetter() {
		setTitle("Operand Getter");
		setFrameIcon(new ImageIcon(
				OperandGetter.class
						.getResource("/gui/icons/Text-Edit-icon.png")));
		setClosable(true);
		setBounds(100, 100, 234, 156);
		getContentPane().setLayout(null);

	}
}
